import React from 'react';
import classnames from "classnames";
import styles from "./App.module.scss";
import Button from 'containers/Button';
import Logs from 'containers/Logs';
import Controls from 'containers/Controls';

export default class App extends React.PureComponent {
  render() {
    return (
      <div className={classnames(styles['app'])}>
        {this.renderButtons()}
        {this.renderLogs()}
        {this.renderControls()}
      </div>
    );
  }

  renderButtons() {
    return <div className={classnames(styles['buttons'])}>
      {[1, 2, 3].map((number) => 
        <Button key={number} number={number}>Button {number}</Button>
      )}
    </div>;
  }

  renderLogs() {
    return <Logs></Logs>;
  }

  renderControls() {
    return <Controls></Controls>;
  }
}