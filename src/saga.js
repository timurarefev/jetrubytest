import { all, spawn } from "redux-saga/effects";
import { saga } from "store/log";

export default function* init(config) {
    yield all([
        spawn(saga, config)
    ]);
}