import {createConnect} from "store/config";
import Controls from "components/Controls";
import { reducer, selectors, actions } from "store/log";

export default createConnect(
    [reducer],
    (store) => ({
        mode: selectors.getMode(store)
    }), {
        reset: actions.reset,
        switchMode: actions.switchMode
    }
)(Controls);