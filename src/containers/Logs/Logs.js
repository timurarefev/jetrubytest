import {createConnect} from "store/config";
import Logs from "components/Logs";
import { reducer, selectors, actions } from "store/log";

export default createConnect(
    [reducer],
    (store) => ({
        logs: selectors.getLogs(store)
    })
)(Logs);