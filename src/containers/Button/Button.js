import {createConnect} from "store/config";
import Button from "components/Button";
import { reducer, selectors, actions } from "store/log";

export default createConnect(
    [reducer],
    (store) => ({
    }),{
        putIntoLogs: actions.putIntoLogs
    }
)(Button);