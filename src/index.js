import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import storeCreator from "store/creator";
import rootSaga from "./saga";
import App from "./App";

const store = storeCreator(rootSaga);

const ROOT = (
    <Provider store={store}>
        <App />
    </Provider>
);

ReactDOM.render(ROOT, document.getElementById("root"));