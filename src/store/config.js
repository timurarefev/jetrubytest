import { createStore as createOriginalStore } from "redux";
import { combineReducers } from "redux-immutable";
import { connect } from "react-redux";
import { Map, fromJS } from "immutable";

let reducersTree = new Map;
let originalStore;

/**
 * Sets reduser in redusers tree
 * @param {*} path 
 * @param {*} reducer 
 */
function setReducerInTree(path, reducer) {
    const keys = [];

    for (let key of path) {
        keys.push(key);

        if (!reducersTree.hasIn(keys))
            break;

        if (typeof reducersTree.getIn(keys) === 'function') {
            if (keys.length === path.length) {
                return false;
            } else {
                throw new Error(`Failed set in path ${path.join(".")} reducer. Path ${keys.join(".")} already used`);
            }
        }
    }

    reducersTree = reducersTree.setIn(path, reducer);

    return true;
}

/**
 * Creates store
 */
export function createStore(initialReducers = {}, ...args) {
    reducersTree = reducersTree.merge(fromJS(initialReducers));

    originalStore = createOriginalStore(combineReducerRecursive(reducersTree.toJS()), ...args);

    return originalStore;
}

export function combineReducerRecursive(reducers) {
    if (typeof reducers === "function")
        return reducers;

    if (typeof reducers === "object") {
        const keys = Object.keys(reducers);

        if (keys.length === 0) {
            return (store) => store;
        }

        for (let key of keys) {
            reducers[key] = combineReducerRecursive(reducers[key]);
        }

        return combineReducers(reducers);
    }

    throw new Error("Invalid reducersTree object");
}

export function injectReducers(injectReducers) {
    let injected = false;

    for (let reducer of injectReducers)
        if (typeof reducer === "object" && Array.isArray(reducer.path) && typeof reducer.reducer === "function") {
            if (setReducerInTree(reducer.path, reducer.reducer))
                injected = true;
        } else
            throw new Error("invalid reducer object");

    injected && originalStore && originalStore.replaceReducer(combineReducerRecursive(reducersTree.toJS()));
}

export function createInjectableReducer(path) {
    if (typeof path === "string")
        return createInjectableReducer(path.split(/\./));

    if (!Array.isArray(path))
        throw new Error("path must be array");

    return (reducer) => {
        if (typeof reducer !== "function") {
            throw new Error("reducer must be function");
        }
        return { path, reducer };
    };
}

export function createConnect(reducers, ...args) {
    if (Array.isArray(reducers))
        injectReducers(reducers);
    else
        throw new Error("reducers must be array");

    return connect(...args);
}