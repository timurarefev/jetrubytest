import { createStore } from "store/config";
import { applyMiddleware, compose } from "redux";
import createSagaMiddleware from "redux-saga";
import { createLogger } from "redux-logger";
import { Map } from "immutable";

export default (rootSaga = null) => {
    const sagaMiddleware = createSagaMiddleware();
    const reducers = {};
    const middleware = [];

    middleware.push(sagaMiddleware);

    if (process.env.NODE_ENV === "development")
        middleware.push(createLogger({
            collapsed: true,
            duration: true,
            stateTransformer: (state) => state.toJSON()
        }));

    const store = createStore(
        reducers,
        new Map,
        compose(
            applyMiddleware(...middleware),
            process.env.NODE_ENV === "development" && window.__REDUX_DEVTOOLS_EXTENSION__
                ? window.__REDUX_DEVTOOLS_EXTENSION__()
                : (f) => f
        )
    );

    if (rootSaga)
        sagaMiddleware.run(rootSaga);

    return store;
}
