import { List } from "immutable";
import { mapToEntity as mapToEntityLog } from "./Log";

export const Logs = List;

export const mapToEntity = (values) => {
    return new Logs(values.map(mapToEntityLog));
}