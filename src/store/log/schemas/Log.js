import { Record } from "immutable";

export const Log = new Record({
    created: null,
    logged: null,
    number: null,
    delay: null,
    isFinished: null,
    started: null
});

export const mapToEntity = (value) => {
    return new Log({
        created: value.created,
        logged: value.logged,
        number: value.number,
        delay: value.delay,
        isFinished: value.isFinished,
        started: value.started
    });
};