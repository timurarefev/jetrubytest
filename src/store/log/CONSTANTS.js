export const SYS_NAMESPACE = "LOG";
export const REDUCER_PATH = "root.log";

export const ACTION_PUT_LOGS = `${SYS_NAMESPACE}:PUT_LOGS`;
export const ACTION_PUT_INTO_LOGS = `${SYS_NAMESPACE}:PUT_INTO_LOGS`;
export const ACTION_RESET = `${SYS_NAMESPACE}:RESET`;
export const ACTION_SWITCH_MODE = `${SYS_NAMESPACE}:SWITCH_MODE`;
export const ACTION_PUT_IS_INTERVAL_STARTED = `${SYS_NAMESPACE}:PUT_IS_INTERVAL_STARTED`;