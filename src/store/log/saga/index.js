import { put, all, takeLatest, select, delay } from "redux-saga/effects";

import {
    ACTION_PUT_INTO_LOGS
} from "store/log/CONSTANTS";

import {
    putLogs,
    putIsIntervalStarted
} from "store/log/actions";

import {
    getLogs,
    getIsIntervalStarted
} from "store/log/selectors";

function* intervalTick() {
    //  Iterator delay. It less than second becourse js setInterval can be not very exact
    let updateInterval = 100;

    //  Iterator
    while (true) {
        //  Delay
        yield delay(updateInterval);

        let isIntervalStarted = yield select(getIsIntervalStarted);

        //  If clock is not active
        if (!isIntervalStarted)
            continue;

        let logs = yield select(getLogs);

        let notFinishedLogs = logs.filter(log => !log.isFinished);

        //  If it doesn't have any unresolved logs
        if (!notFinishedLogs || !notFinishedLogs.size)
        {
            yield put(putIsIntervalStarted(false));
            continue;
        }

        //  Update logs list if it's time to update
        let updatedLogs = logs
            .map(data => {
                //  If it's already resolved
                if (!!data.isFinished)
                    return data;

                let logged = new Date();

                //  If it time to set logged time
                if (((logged.getTime() - data.started.getTime()) / 1000) >= data.delay)                
                    return Object.assign(data.toJS(), {logged, isFinished: true});

                //  If it's not
                return data;
            });

        if (updatedLogs != logs)
            yield put(putLogs(updatedLogs));
    }
}

function* startInterval() {
    //  When button clicks it starts iterator
    yield put(putIsIntervalStarted(true));
}

export default function* init(config) {
    yield all([
        takeLatest([
            ACTION_PUT_INTO_LOGS
        ], startInterval, config),
        intervalTick(config)
    ])
}