import { List } from "immutable";
import { REDUCER_PATH } from "store/log/CONSTANTS";

export const getLogs = (store, reducerPath = REDUCER_PATH) => {
    const storePath = reducerPath.split(/\./).concat('logs');

    if (store.hasIn(storePath))
        return store.getIn(storePath);

    return new List;
}

export const getMode = (store, reducerPath = REDUCER_PATH) => {
    const storePath = reducerPath.split(/\./).concat('mode');

    if (store.hasIn(storePath))
        return store.getIn(storePath);

    return 'easy';
}

export const getIsIntervalStarted = (store, reducerPath = REDUCER_PATH) => {
    const storePath = reducerPath.split(/\./).concat('isIntervalStarted');

    if (store.hasIn(storePath))
        return store.getIn(storePath);

    return false;
}