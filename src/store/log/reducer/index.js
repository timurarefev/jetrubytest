import { Map, List } from "immutable";
import { mapToEntity } from "store/log/schemas/Logs";
import { createInjectableReducer } from "store/config";

import {
    REDUCER_PATH,
    ACTION_PUT_INTO_LOGS,
    ACTION_PUT_LOGS,
    ACTION_SWITCH_MODE,
    ACTION_RESET,
    ACTION_PUT_IS_INTERVAL_STARTED
} from "store/log/CONSTANTS";

const initialState = new Map({
    logs: new List,
    mode: 'easy'
});

export default createInjectableReducer(REDUCER_PATH)(
    function (state = initialState, action) {
        let logs, mode;

        switch (action.type) {
            case ACTION_PUT_IS_INTERVAL_STARTED:
                //  Is clock now active
                return state.setIn(['isIntervalStarted'], action.payload.isIntervalStarted);

            case ACTION_SWITCH_MODE:
                //  If it easy or hard mode of test
                mode = state.hasIn(['mode']) ? state.getIn(['mode']) : 'easy';

                return state.setIn(['mode'], mode === 'easy' ? 'hard' : 'easy');

            case ACTION_RESET:
                //  Resets app
                return state
                .setIn(['mode'], 'easy')
                .setIn(['logs'], new List)
                .setIn(['isIntervalStarted'], false);

            case ACTION_PUT_LOGS:  
                //  Replace all logs with new values
                logs = action.payload.logs;
                logs = mapToEntity(logs);
                    
                return state.setIn(['logs'], logs);

            case ACTION_PUT_INTO_LOGS:
                //  Put new log after button click
                mode = state.hasIn(['mode']) ? state.getIn(['mode']) : 'easy';
                logs = state.hasIn(['logs']) ? state.getIn(['logs']) : new List;

                let created = new Date();
                let started = new Date(created.getTime());

                if (mode === 'hard' && !!logs && !!logs.size)
                    started.setTime(Math.max(logs.reduce((accumulator, value) => Math.max(accumulator, (value.started.getTime() / 1000) + value.delay), 0) * 1000, created.getTime()));

                return state.setIn(['logs'], mapToEntity(logs.push({
                    number: action.payload.log.number,
                    delay: Math.round(Math.random() * 9) + 1,
                    isFinished: false,
                    logged: null,
                    created,
                    started
                })));

            default:
                return state;
        }
    }
);