import {
    ACTION_PUT_INTO_LOGS,
    ACTION_PUT_LOGS,
    ACTION_RESET,
    ACTION_SWITCH_MODE,
    ACTION_PUT_IS_INTERVAL_STARTED
} from "store/log/CONSTANTS";

export function putIsIntervalStarted(isIntervalStarted) {
    return {
        type: ACTION_PUT_IS_INTERVAL_STARTED,
        payload: { isIntervalStarted }
    }
}

export function putIntoLogs(log) {
    return {
        type: ACTION_PUT_INTO_LOGS,
        payload: { log }
    }
}

export function putLogs(logs) {
    return {
        type: ACTION_PUT_LOGS,
        payload: { logs }
    }
}

export function reset() {
    return {
        type: ACTION_RESET
    }
}

export function switchMode() {
    return {
        type: ACTION_SWITCH_MODE
    }
}