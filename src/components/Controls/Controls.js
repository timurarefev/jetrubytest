import React from 'react';
import classnames from "classnames";
import styles from "components/Controls/Controls.module.scss";
import Button from 'containers/Button';

export default class Controls extends React.PureComponent {
  render() {
    const { mode } = this.props;

    return (
      <div className={classnames(styles['controls'])}>
        <Button onClick={this.reset}>Reset</Button>
        <Button className={classnames(styles[mode])} onClick={this.switchMode}>Switch to {mode === 'hard' ? 'easy' : 'hard'} mode</Button>
      </div>
    );
  }

  reset = (e) => {
    const { reset } = this.props;

    !!reset && reset();
  }

  switchMode = (e) => {
    const { switchMode } = this.props;

    !!switchMode && switchMode();
  }
};