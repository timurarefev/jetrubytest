import React from "react";
import styles from "components/Button/Button.module.scss";
import classnames from "classnames";

export default class Button extends React.PureComponent {
    render() {
        const { children, className } = this.props;

        return (
            <div onClick={this.handleClick} className={classnames(styles['button'], !!className && className)}>
                {children}
            </div>
        );
    }

    handleClick = (e) => {
        e.preventDefault();

        const { onClick, putIntoLogs, number } = this.props;

        !!onClick && onClick();

        if (!!onClick)
            return;
        
        !!putIntoLogs && putIntoLogs({number});
    }
}