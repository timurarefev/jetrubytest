import React from 'react';
import classnames from "classnames";
import styles from "components/Logs/Logs.module.scss";

export default class Logs extends React.PureComponent {
  render() {
    const { logs } = this.props;

    let finishedLogs = logs.filter(log => !!log.isFinished);
    
    //, время старта {log.started.toLocaleString("ru-RU")}

    return (
      <div className={classnames(styles['logs'])}>
        {!!finishedLogs && !!finishedLogs.size && finishedLogs.map(log => {
          return <div>{log.logged.toLocaleString("ru-RU")}: Нажата кнопка {log.number}, задержка {log.delay}, время нажатия {log.created.toLocaleString("ru-RU")}</div>;
        })}
      </div>
    );
  }
};